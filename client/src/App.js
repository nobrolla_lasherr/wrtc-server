import React, { Component } from 'react';
import './App.css';

import WebrtcClient from './components/WebrtcClient'

class App extends Component {
  render() {
    return (
      <div className="App">
        <WebrtcClient webSocketUrl="ws://192.168.46.41:8080" />
      </div>
    );
  }
}

export default App;
