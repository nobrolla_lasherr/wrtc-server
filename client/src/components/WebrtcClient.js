import React, { Component } from 'react';

class WebrtcClient extends Component {

  constructor(props) {
    super(props)

    this.webSocketConnection = null;
  
    this.state = {
       
    }
  }

  //callback for sdp offer created
  onOfferCreated = (description) => {
    console.log("sending offer")
    this.rtcPeerConnection.setLocalDescription(description);
    this.webSocketConnection.send(JSON.stringify({type: 'offer', payload: description}));
  }

  //callback for when the ws is oppened
  onWebSocketOpen = () => {
    console.log("open");
    const config = {};
    this.rtcPeerConnection = new RTCPeerConnection(config);

    const dataChannelConfig = { ordered: false, maxRetransmits: 0 };
    var dataChannel = this.rtcPeerConnection.createDataChannel('dc', dataChannelConfig);

    dataChannel.onmessage = this.onDataChannelMessage;
    dataChannel.onopen = this.onDataChannelOpen;
    this.rtcPeerConnection.onicecandidate = this.onIceCandidate;

    const sdpConstraints = {
      mandatory: {
        OfferToReceiveAudio: false,
        OfferToReceiveVideo: false,
      },
    };
    this.rtcPeerConnection.createOffer(this.onOfferCreated, () => {}, sdpConstraints);
  }

  //callback for receiving ws data
  onWebSocketMessage = (event) => {
    console.log(event);
  }

  componentDidMount() {
    this.webSocketConnection = new WebSocket(this.props.webSocketUrl);
    this.webSocketConnection.onopen = this.onWebSocketOpen;
    this.webSocketConnection.onmessage = this.onWebSocketMessage;
  }
  
  
  render() {
    return (
      <div>
        <h1>Client</h1>
      </div>
    );
  }
}

export default WebrtcClient;
